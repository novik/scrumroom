﻿using System;
using System.Collections.Generic;

namespace ScrumRoom.Models
{
    public partial class RoomUser
    {
        public int Int { get; set; }
        public int RoomId { get; set; }
        public int UserId { get; set; }
        public int RoleId { get; set; }

        public virtual Role Role { get; set; }
        public virtual Room Room { get; set; }
        public virtual User User { get; set; }
    }
}
