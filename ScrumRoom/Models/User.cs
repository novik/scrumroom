﻿using System;
using System.Collections.Generic;

namespace ScrumRoom.Models
{
    public partial class User
    {
        public User()
        {
            RoomUser = new HashSet<RoomUser>();
            Vote = new HashSet<Vote>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Pass { get; set; }

        public virtual ICollection<RoomUser> RoomUser { get; set; }
        public virtual ICollection<Vote> Vote { get; set; }
    }
}
