﻿using System;
using System.Collections.Generic;

namespace ScrumRoom.Models
{
    public partial class Vote
    {
        public int Id { get; set; }
        public int IssueId { get; set; }
        public int UserId { get; set; }
        public int? Vote1 { get; set; }

        public virtual Issue Issue { get; set; }
        public virtual User User { get; set; }
    }
}
