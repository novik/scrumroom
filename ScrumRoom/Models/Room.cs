﻿using System;
using System.Collections.Generic;

namespace ScrumRoom.Models
{
    public partial class Room
    {
        public Room()
        {
            Issue = new HashSet<Issue>();
            RoomUser = new HashSet<RoomUser>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime Created { get; set; }
        public bool Active { get; set; }
        public string CustomValues { get; set; }

        public virtual ICollection<Issue> Issue { get; set; }
        public virtual ICollection<RoomUser> RoomUser { get; set; }
    }
}
