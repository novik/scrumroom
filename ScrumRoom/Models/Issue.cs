﻿using System;
using System.Collections.Generic;

namespace ScrumRoom.Models
{
    public partial class Issue
    {
        public Issue()
        {
            Vote = new HashSet<Vote>();
        }

        public int Id { get; set; }
        public int RoomId { get; set; }
        public int? StatusId { get; set; }
        public string Name { get; set; }
        public string Link { get; set; }
        public string Comments { get; set; }
        public int? Estimated { get; set; }

        public virtual Room Room { get; set; }
        public virtual IssueStatus Status { get; set; }
        public virtual ICollection<Vote> Vote { get; set; }
    }
}
