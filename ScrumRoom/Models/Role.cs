﻿using System;
using System.Collections.Generic;

namespace ScrumRoom.Models
{
    public partial class Role
    {
        public Role()
        {
            RoomUser = new HashSet<RoomUser>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<RoomUser> RoomUser { get; set; }
    }
}
