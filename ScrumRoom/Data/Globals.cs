﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ScrumRoom.Data
{
    public static class Globals
    {
        public static string ConnectionString { get; set; }
    }
}
